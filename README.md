# Getting Started

## Clone this project

* Clone this project
* remove the git relation
* init a new git repository

```bash
mkdir new_atlassian_project_folder
cd new_atlassian_project_folder
git clone https://bitbucket.org/cmbaer/atlassian-project-template.git .
rm -Rf .git
git init .
git add --all
git commit -m "Initial Commit"
```

## Install roles

see [Ansible Documentation](https://galaxy.ansible.com/docs/using/installing.html#determining-where-roles-are-installed)

```bash
ansible-galaxy install --roles-path roles -r requirements.yml
```

## Update vagrantfile

Since the release of Apples M cpu machines I had to switch from virtualbox to parallels and need to use arm64 based boxes which are by now not available for redhat based distros, so I had to switch to ubuntu also

So the old Vagrantfile and Vagrantfile.template are now Vagrantfile.intel and Vagrantfile.intel.template

For simple use, I've added a ready to use *Vagrantfile*

With *Vagrantfile.template.intel* or *Vagrantfile.template.arm64* you have some placeholders to replace with your own settings.

### Vagrantfile.template

* \<set system name here> needs to be replaced 2 times by your system name

* \<set system ip here> is a local ip, e.g. 192.168.10.10


my personal setting ( to use one software folder on host for all vm):

```node.vm.synced_folder "~/Atlassian/Software", "/opt/softwarepackages"```

Relative paths are also possible:

```node.vm.synced_folder "../../atlassian/software", "/opt/softwarepackages"```


## Start up

Start the vm with

    vagrant up

for use of hostmanager ( which sets /etc/hosts) you need to provide your passwort, so keep a look at the output ;-)

## Setup Jira and Confluence

Go to http://atlas:8080 for jira or http://atlas:8090 for confluence.

Follow the setup routine providing a (evaluation) license and use the build in evaluation database 





